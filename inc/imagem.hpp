#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <string>
#include <fstream>
#include <istream>

using namespace std;

class Imagem
{
  private:
    char *arquivo;
    string tipo;
    string caminho;
    int largura;
    int altura;
    int valor_maximo;

  public:
    Imagem();
    ~Imagem();
    void setImagem(char* arquivo);
    void setTipo(string tipo);
    void setEndereco(string caminho);
    void setLargura(int largura);
    void setAltura(int altura);
    void setValorMaximo(int valor_maximo);

    char *getArquivo();
    string getTipo();
    string getEndereco();
    int getLargura();
    int getAltura();
    int getValorMaximo();
    void le_cabecalho(string caminho);
};

#endif
