#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP
#include "imagem.hpp"

class imagemPPM : public Imagem
{
	private:
		char *Vermelho;
		char *Verde;
		char *Azul;

	public:
		imagemPPM();
		~imagemPPM();

		char *getVermelho();
		char *getVerde();
		char *getAzul();
		void setVermelho(char *Vermelho);
		void setVerde(char *Verde);
		void setAzul(char *Azul);
    void le_cabecalho(string caminho);
		void atribuiRGB();
		void aplicaFiltro();
		void salvaImagem();

};
#endif
