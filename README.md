Explicação de como compilar o Exercício de Programação 1:

  O projeto foi disposto em uma estrutura de pastas:
    ->bin: pasta do binário;
    ->doc: pasta com as imagens para uso, e diretório das imagens de saída;
    ->inc: pasta dos headers;
    ->obj: pasta dos arquivos objetos;
    ->src: pasta das implementações.

  Para compilar basta usar o mecanismo do arquivo MakeFile, utilizando os seguintes comandos:
    1- make;
    2- make run;

  A partir de então basta seguir as instruções dadas pelo programa. Exemplo:
    Bem vindo ao decifrador de esteganografia!!
    Digite o nome/caminho da imagem:
      ./doc/mensagem2.ppm
    Qual o formato da imagem? (ppm/pgm):
      ppm
    Digite a cor predominante na imagem(vermelho,azul,verde):
      verde
    Então vamos aplicar o filtro verde!!
    Digite o nome do arquivo de saída:(digite da seguinte forma : nome_do_arquivo.formato)
      saida2.ppm
    doc/saida2.ppmPRONTO!!! SEU FILTRO FOI APLICADO COM SUCESSO!
