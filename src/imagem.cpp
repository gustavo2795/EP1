#include "imagem.hpp"

using namespace std;

Imagem::Imagem()
{
  tipo= "";
  caminho="";
  altura = 0;
  largura = 0;
}

Imagem::~Imagem()
{

}

void Imagem::setImagem(char *arquivo)
{
	this->arquivo = arquivo;
}

void Imagem::setTipo(string tipo)
{
  this->tipo = tipo;
}

void Imagem::setEndereco(string caminho) {
	this->caminho = caminho;
}

void Imagem::setLargura(int largura)
{
  this->largura = largura;
}

void Imagem::setAltura(int altura)
{
  this->altura = altura;
}

void Imagem::setValorMaximo(int valor_maximo)
{
  this->valor_maximo = valor_maximo;
}

char* Imagem::getArquivo()
{
	return arquivo;
}

string Imagem::getEndereco(){
	return caminho;
}

string Imagem::getTipo()
{
  return tipo;
}

int Imagem::getLargura()
{
  return largura;
}

int Imagem::getAltura()
{
  return altura;
}

int Imagem::getValorMaximo()
{
	return valor_maximo;
}

void Imagem::le_cabecalho(string caminho)
{
  char c;

	int i = 0;

  fstream file(caminho.c_str());

  if (file == NULL)
  {
    cout << "Não Existe" << endl;
  }
  else
	{
    getline(file, tipo);

		while(1)
		{
			c = file.peek();

			if(c == '#')
			{
				file.ignore(512, '\n');
			}else
			{
				break;
			}
		}

	  file >> altura;
    file >> largura;
	  file >> valor_maximo;

		arquivo = new char[(getAltura() * getLargura()) + 1];

	  while(i < (getAltura() * getLargura()) + 1)
		{
			file.get(c);
			arquivo[i] = c;
			i++;
		}
	}
}
