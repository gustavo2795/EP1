#include "imagemPPM.hpp"
#include "imagem.hpp"

imagemPPM::imagemPPM()
{
  setEndereco(" ");
  setTipo(" ");
  setAltura(0);
  setLargura(0);
  setValorMaximo(255);
	Vermelho = 0;
	Verde = 0;
	Azul = 0;
}

imagemPPM::~imagemPPM()
{
}

char* imagemPPM::getVermelho()
{
	return Vermelho;
}
char* imagemPPM::getVerde()
{
	return Verde;
}

char* imagemPPM::getAzul()
{
	return Azul;
}

void imagemPPM::setVermelho(char *Vermelho)
{
	this->Vermelho = Vermelho;
}

void imagemPPM::setVerde(char *Verde)
{
	this->Verde = Verde;
}

void imagemPPM::setAzul(char *Azul)
{
	this->Azul = Azul;
}

void imagemPPM::le_cabecalho(string caminho) //Polimorfismo
{
  char c;
	char *imagem;
	string tipo;
	int altura, largura, valor_maximo;
	int i = 0;

    fstream file(caminho.c_str());

    if(file == NULL)
    {
      cout << "Erro ao ler a imagem" << endl;
    }
    else
	  {
      getline(file, tipo);
		  while(1)
		  {
			  c = file.peek();

			  if(c == '#')
			  {
				  file.ignore(512, '\n');
			  }else
			  {
				break;
			  }
		  }

	    file >> altura;
      file >> largura;
		  file >> valor_maximo;

		  imagem = new char[(altura * largura) * 3];

	    while(i < (altura * largura) * 3)
		  {
			  file.get(c);
		    imagem[i + 2] = c;
        i++;
		  }
	  }

	setTipo(tipo);
	setAltura(altura);
	setLargura(largura);
	setValorMaximo(valor_maximo);
	setImagem(imagem);
}

void imagemPPM::atribuiRGB()
{
	char *cImagem;
	int i = 0, j = 0;
	int numPixels;

	numPixels = (getAltura() * getLargura()) * 3;

	cImagem = new char[(getAltura() * getLargura()) * 3];
	cImagem = getArquivo();


	Vermelho = new char[(getAltura() * getLargura())];
	Verde = new char[(getAltura() * getLargura())];
	Azul = new char[(getAltura() * getLargura())];


	while(i < numPixels)
	{
		Vermelho[j] = cImagem[i];
		i++;
		Verde[j] = cImagem[i];
		i++;
		Azul[j] = cImagem[i];
		i++;
		j++;
	}

}

void imagemPPM::aplicaFiltro()
{

	int i = 0, j = 0;
	char *cImagem;
  string cor_predominante;

	cout << "Digite a cor predominante na imagem(vermelho,azul,verde):" << endl;

	do
	{
		cin >> cor_predominante;
	}while(cor_predominante != "vermelho" && cor_predominante != "azul" && cor_predominante != "verde");

  if(cor_predominante == "vermelho")
  {
    cout << "Então vamos apicar o filtro vermelho!!" << endl;
    for(int i = 0 ; i < (getAltura() * getLargura()) ; i++)
    {
      Verde[i] = 0;
      Azul[i] = 0;
    }
  }
  else if(cor_predominante == "verde")
  {
    cout << "Então vamos aplicar o filtro verde!!" << endl;
    for(int i = 0 ; i < (getAltura() * getLargura()) ; i++)
    {
      Vermelho[i] = 0;
      Azul[i] = 0;
    }
  }
  else
  {
    cout << "Então vamos aplicar o filtro azul!!" << endl;
    for(int i = 0 ; i < (getAltura() * getLargura()) ; i++)
    {
      Vermelho[i] = 0;
      Verde[i] = 0;
    }
  }

	cImagem = new char[(getAltura() * getLargura()) * 3];

	while(i < (getAltura() * getLargura()) * 3)
	{
		cImagem[i] = Vermelho[j];
    i++;
    cImagem[i] = Verde[j];
    i++;
    cImagem[i] = Azul[j];
    i++;
    j++;
	}
	setImagem(cImagem);
}

void imagemPPM::salvaImagem()
{
	string nomeArquivo;

	cout << "Digite o nome do arquivo de saída:" << endl << "(digite da seguinte forma : nome_do_arquivo.formato)" << endl;

	cin >> nomeArquivo;
	nomeArquivo = "doc/" + nomeArquivo;

	cout << nomeArquivo;

	ofstream saida(nomeArquivo.c_str());

	saida << getTipo() << endl;
	saida << getAltura() << " " << getLargura() << endl;
	saida << getValorMaximo() << endl;

	saida.write(getArquivo(), (getAltura() * getLargura()) * 3);
	cout << "PRONTO!!! SEU FILTRO FOI APLICADO COM SUCESSO!" << endl;
}
