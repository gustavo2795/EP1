#include "imagemPPM.hpp"
#include "imagem.hpp"

using namespace std;

int main()
{
	string caminho;
	string tipo;
	cout << "Bem vindo ao decifrador de esteganografia!!" << endl;
	cout << "Digite o nome/caminho da imagem:" << endl;
	cin >> caminho;

	cout << "Qual o formato da imagem? (ppm/pgm):" << endl;
	cin >> tipo;

	if(tipo == "ppm")
	{
		imagemPPM imagemFornecida;
		imagemFornecida.le_cabecalho(caminho);
		imagemFornecida.atribuiRGB();
		imagemFornecida.aplicaFiltro();
		imagemFornecida.salvaImagem();
	}
	else if(tipo == "pgm")
	{

	}
	else
	{
		cout << "Formato digitado é inválido, tente novamente!" << endl;
	}

	return 0;
}
